class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 17 - Classification using Machine Learning </h1>

<h3 class="title_headings_sml"> Dr. Mitchell Welch </h3>

---

## Reading

* Chapter 3 from ***Hands-on Machine Learning with Scikit-Learn & TensorFlow***

---
## Summary

* Machine Learning & Classification
* MNIST Dataset
* Partitioning the Data Set
* Training the Classifier
---

## Machine Learning & Classification

* Recall from the previous topics that the most common supervised learning tasks are:
    * Regression (predicting values)
    * Classification (predicting category classes)
* In machine learning, classification refers to a predictive modeling problem where a class label is predicted for a given example of input data.
* Examples: Activity recognition from accelerometer sensors, natural language processing, plant species classification. 
---

## Machine Learning & Classification

.center[![Matplotlib](assets/topic_12/ml.png)]


---
## Machine Learning & Classification

* Classification requires a training dataset with **many** examples of inputs and outputs from which to learn.
* *Binary classification* refers to those classification tasks that have two class labels.
* *Multi-class classification* refers to those classification tasks that have more than two class labels.

---

## MNIST Dataset

* To continue our discussion on machine learning, we will introduce the *MNIST* dataset.
* The MNIST dataset contains 70,000 small images of digits handwritten by high school students and employees of the US Census Bureau.
* Each image is labeled with the digit it represents.
* The digit images (technically the pixels from the images) are the input features and the ground-truth labels are the output.
* We will train some classifier algorithms to predict the digit given the image. 


---

## MNIST Dataset

* A copy of the MNIST dataset is available within Scikit-learn.
* Review the demonstration notebook to how to load and preview the data.
* There are 70,000 images, and each image has 784 features.
* This is because each image is 28 × 28 pixels, and each feature simply represents one pixel’s intensity, from 0 (white) to 255 (black).

---
## MNIST Dataset

.center[![Matplotlib](assets/topic_17/mnist.png)]

---

## Partitioning the Data Set

* Before we move on to train our classifier, we need to divide our data set into a *training* and *testing* set
* The training set is used to train and tune the classifier.
* The test set is used to test the predictive ability of the classifier.
* None of the data items in the test set should be used as part of the training process. 

---

## Partitioning the Data Set

* If we look at the MNIST dataset packaged with Scikit-Learn, we can see it has test/train partitions built in:
  * The first 60000 images make up the training set.
  * The last 10000 images make up the testing set.
* This partitioning ensures that there are equal examples of each digit within each set.

```python 
X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]
```

---

## Training the Classifier

* Now we can investigate some classifier algorithms.
* Let’s simplify the problem for now and only try to identify one digit—for example, the number "5"
* This “5-detector” will be an example of a binary classifier, capable of distinguishing between just two classes, "5" and "not-5"

---
## Training the Classifier

* We will train a linear classifier using Stochastic Gradient Descent (SGD) algorithm.
* Strictly speaking, SGD is merely an optimization technique and does not correspond to a specific family of machine learning models.
* We will be using the default model: the *Support Vector Machine (SVM)*
* In a later topic we will cover the workings of the SVM.
---

## Training the Classifier

* The Support Vector Machine (SVM) works by constructing a *hyperplane* - essentially a decision boundary that separates the classes within the data.
* This algorithm is very efficient and works well on data the is *linearly separable* (classes that can be separated by a straight line within the features)
* This classifier has the advantage of being capable of handling very large datasets efficiently.

---
## Training the Classifier

.center[![Matplotlib](assets/topic_17/svm.png)]

---
## Training the Classifier

* Now its time to actually train the classifier using the training set.

```python
from sklearn.linear_model import SGDClassifier
sgd_clf = SGDClassifier(random_state=42)
sgd_clf.fit(X_train, y_train_5)

# A quick test on our classifier
sgd_clf.predict([X[0]])

```

---
## Summary

* Machine Learning & Classification
* MNIST Dataset
* Partitioning the Data Set
* Training the Classifier

---

## Reading

* Chapter 3 from ***Hands-on Machine Learning with Scikit-Learn & TensorFlow***

---




