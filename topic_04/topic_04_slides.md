
class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 3 - Starting Out With Python #1
</h1>

<h3 class="title_headings_sml"> Andrew Shepley </h3>

---

## What is Python?

* Python is a high-level language
* Other high-level languages include Java, C++, Perl and others
* Low-level languages are computer languages, sometimes called ‘assembly languages’
* Benefits of high-level languages:
  * Portable: they can run on any computer
  * They are easier to program with – short and easier to read
  * Easier to maintain
* Shortcomings:
  * Slower because they must be translated into machine language by a compiler or an interpreter

---

## What is Python? cont…

* Python is an interpreted language 
  * Interpretation means the program is converted to machine language line by line
  * Slower
  * Platform independent 
  * Can be modified while running
  * Other interpreted languages include C#, Javascript, and Ruby
* Non-interpreted languages are compiled
  * Compilation means the program is converted to machine language all at once
  * Faster 
  * Examples include C, and C++
  * Platform dependent

---

## What is Python? cont…

* Other benefits of Python include:
  * It is a multi-purpose language
  * It is highly readable making it:
      * Easy to program
      * Take less time to write programs, 
      * Programs are shorter
  * Large range of libraries, especially for data science, e.g. sci-kit
  * Great for IoT development
Shortcomings
  * Watch the indentation! 
  * Slower than other languages, e.g Swift, C++


---

## Welcome to COSC102

.center[![Data Science Venn](assets/topic_01/studio.png "Studio Unit")]

---

## What is Data Science?

* There’s a joke that says:
> A data scientist is someone who knows more statistics than a computer scientist and more computer science than a
statistician.
* Loosely defined mix of **Computer Science**, **Statistics**, **Mathematics** and **Domain Knowledge**.
  * Data Acquisition and problem solving
  * Software Development and Automation
  * Machine Learning and Computer Modelling
  * Deployment and Engineering

---

## What is Data Science?

.center[![Data Science Venn](assets/topic_01/data_science_venn_diagram.png "DS Venn")]

---

## What We Will Study?

* Introductory Machine learning.
* Data Visualization.
* Data Cleaning/Data Wrangling.
* Python Programming.
* Basic Data Formats/Structures.

---

## What We Will Study?

* Construct scripts and software.
* Working Collaboratively on a Data Science Project.
* Programming and Problem solving through Data Science.
* Providing Peer Feedback.

---
## What We Will Study?

* Textbook: *Hands-On Machine Learning with Scikit-Learn, Keras, and Tensorflow: Concepts, Tools, and Techniques to Build Intelligent Systems*

.center[
    ![Data Science Venn](assets/topic_01/text_1st.png "DS Venn")
    ![Data Science Venn](assets/topic_01/text_2nd.png "DS Venn")

]

---

## Unit overview

* All resources for this unit are on the [COSC102 Moodle Page](https://moodle.une.edu.au/course/view.php?id=20952) 
* The [Study Timetable](https://moodle.une.edu.au/mod/book/view.php?id=1862764&chapterid=529304) is tentative at this point.
* The is a tile for each week of the trimester (12 in total)
* Review all of the videos for each topic.
* Complete the tutorial exercises for each week. 

---
## Unit overview

* Unit staff

.center[![Data Science Venn](assets/topic_01/uc.png "DS Venn")]

---

## Lets Play!

* Simple machine learning demo using the [*Iris Dataset*]()
* The data set consists of 50 samples from each of three species of Iris (*Iris setosa*, *Iris virginica* and *Iris versicolor*)
*  Four features were measured from each sample: the **length** and the **width** of the **sepals** and **petals**, in centimeters.
*  We will train a simple machine learning algorithm to classify the species from the provided features.
*  [Download the code](http://turing.une.edu.au/~cosc102/topics/topic_01/examples/plot_classification.py)
* Lets review the demo using myBinder:


.center[[![Binder Knn demo](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_01%2Fknn_demo.ipynb)]


---

## Reading

* Review/Explore the COSC102 [Moodle Page](https://moodle.une.edu.au/course/view.php?id=20952)

---
