
class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 4 - Starting Out With Python #2
</h1>

<h3 class="title_headings_sml"> Andreas Shepley, PhD Candidate </h3>

---

## In this Video...

* We'll be looking at:
  * if statements
  * for and while loops
  * continue and break

*  [Here is the link to the Jupyter Notebook we'll be using in this video](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_04%2FTopic%204%20Starting%20Out%20with%20Python%202.ipynb)

---

## if statements

* If statements allow you to execute code if a given condition is True.
* If statements are booleans. They are always True or False.
* **Format**
.center[![if](assets/topic_04/slide_if.PNG "if")]
* If the condition is True, the indented code will execute. If it is False, the indented code will be skipped.

---

## elif and else statements

* elif statements allow you to test for multiple conditions.
* else statements allow you to execute code when none of your conditions are satisfied.
* else can be used with both if and elif conditions.
* **Format**
.center[![elif](assets/topic_04/slide_if_elif_else.PNG "if_elif_else")]

---

## Repetition: for loops

* for loops iterate over each element in a collection
* for loops allow you to perform operations on each element in a collection
* **Format**
.center[![for](assets/topic_04/slide_for.PNG "for")]
---

## Repetition: while loops
* while loops allow you to perform one or more operations for as long as a condition is True
* **Format**
.center[![while](assets/topic_04/slide_while.PNG "while")]
---

##continue

* The continue keyword allows you to skip performing operations on a specific element in a collection
* **Format**
.center[![continue](assets/topic_04/slide_continue.PNG "continue")]
---

## break

* break statements allow you to exit a for or while loop prematurely
* break statements are useful in preventing endless loops
* **Format**
.center[![break](assets/topic_04/slide_break.PNG "break")]	
---

## Let's Play!

* Here is the mybinder link to the Jupyter Notebook you've watched me use in this video
  *  [Visit the Jupyter Notebook](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_04%2FTopic%204%20Starting%20Out%20with%20Python%202.ipynb)
* Remember programming is all about practical experience so feel free to experiment and test things out in the notebook

* Happy coding!
.center[[![Binder Python Lesson 1](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_04%2FTopic%204%20Starting%20Out%20with%20Python%202.ipynb)]


---
## Next Topic...
* In the next video, we'll be covering:
  * slicing and dicing lists and strings
  * for loops with enumerate and zip
  * a for loop in one line of code!

* Thanks for watching! If you have any questions, ask away on SLACK.

