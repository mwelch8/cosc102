class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 20 - Multiclass Classification </h1>

<h3 class="title_headings_sml"> Dr. Mitchell Welch </h3>

---


## Reading

* Chapter 3 from ***Hands-on Machine Learning with Scikit-Learn & TensorFlow***

---

## Summary

* Multiclass Classification
* Coded Demonstrations

---

## Multiclass Classification

* So far we have only looked at a binary problem: building a "5" detector for the MNIST dataset.
* We will expand on this with a full multiclass classifier.
* Recall that we have already divided the dataset and created the full multiclass dataset for training.
* This make things very easy:

```python
sgd_clf.fit(X_train, y_train)
sgd_clf.predict([some_digit])
```

---

## Multiclass Classification

* Some machine learning algorithms are capable of multiclass classification without the need for additional processing. (e.g. Random forest, Naive bayes)
* Others are strictly binary classifiers that require additional strategies to extend them for the multi-class problem.
  * One-vs-rest - train one classifier for each class
  * One-vs-one - train one classifier for each distinct pair.
* As part of the classification process, a result is calculated from multiple binary classifiers. 

---

## Coded Demonstrations

* To demonstrate the use of multiclass classification, we are going to review two coded examples using the *iris* dataset.
  * [Example 1]()
  * [Example 2]()

---

## Summary

* Multiclass Classification
* Coded Demonstrations

---