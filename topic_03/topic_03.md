
class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 3 - Starting Out With Python #1
</h1>

<h3 class="title_headings_sml"> Andreas Shepley, PhD Candidate </h3>

---


## In this Video...

* We'll be covering the following:
  * What is Python?
  * What is a program? 
  * What are variables?
  * What are datatypes and operators?

* [Click here to access the Jupyter Notebook used in this video](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_03%2FTopic%203%20Starting%20Out%20With%20Python%201.ipynb)

---

## What is Python?

* Python is a high-level language, just like Java, C++, Perl and others
* Low-level languages are computer languages, sometimes called ‘assembly languages’
* **Benefits of high-level languages**
  * Portable: they can run on any computer
  * They are easier to program with – short and easy to read
  * Easier to maintain
* **Shortcomings**
  * Slower because they must be translated into machine language by a compiler or an interpreter


---

## What is Python? cont…

* Python is an *interpreted language* 
  * Interpretation means the program is converted to machine language line by line
  * Slower
  * Platform independent 
  * Other interpreted languages include C#, Javascript, and Ruby
* Non-interpreted languages are compiled
  * Compilation means the program is converted to machine language all at once
  * Faster 
  * Examples include C, and C++
  * Platform dependent


---

## What is Python? cont…

* **Other benefits of Python include:**
  * It is a multi-purpose language
  * It is highly readable meaning:
      * It is easy to program
      * It takes less time to write programs, 
      * Programs are shorter
  * Python has a large range of libraries, especially for data science, e.g. scikit
* **Shortcomings**
  * Watch the indentation! 
  * Slower than other languages, e.g Swift, C++


---

## What is a program?
* A program is a set of instructions followed in sequence by a computer to solve a problem
* Programs use variables, algorithms and functions
* Algorithms are fundamental components of programs designed to achieve specific goals
* Programs are usually made of multiple algorithms working together to solve a bigger problem
* Algorithms are constructed using variables and functions

---

##What is a Function? 
* Functions take input and return output
* A specific task in a program can be performed by a function
* For example. We could create a function to convert a student's grade to a percentage:
  * **Input:** *number of marks awarded*=40, *total marks*=50
  * **Function process:** *percentage* = (40/50)*100
  * **Output:** 80%
---

## What is a variable?

* A variable is the name of a value 
* Variables should have meaningful names
  * Can include numbers, underscores and letters
  * Should relate to the meaning of the value
    * e.g. assignment_grade = 78 is better than a_g = 78
* You can’t start a variable name with a number
  * e.g. 0student = ‘James’ won’t work
* Variables are case sensitive
  * e.g. Student = ‘Mary’ is different to student = ‘Mary’

---

## Python Reserved Keywords
* These python reserved keywords cannot be used as variable names. They have special meaning to the interpreter. 

.center[![Python Reserved Keywords](assets/topic_03/python_reserved_keywords_resize.png)]

---

## Python Datatypes 

.center[![Datatypes](assets/topic_03/data_types.PNG)]

* In these videos, we're going to use: *str, int, float, list, dict, set, and bool*

---

## Python Datatypes cont...
* **int:** Integer numbers (no decimal places)
 * For example: 5, 800, 0
 * Useful for representing categorical data
 * Example of usage: number_dogs = 6
* **float:** Floating point numbers (decimal places)
 * For example: 5.0, 89.4, 100.95
 * Example of usage: petrol_price = 1.567

---

##Python Datatypes cont...

* **string**: Strings such as words, characters and phrases 
 * For example: “Bob”, “He took the knife”, “butter chicken”
 * Useful for representing words, sentences or letters
 * Example of usage: student_name = “Ahmed”
* **bool**: Boolean 
 * True or False
 * Useful for conditional variables, where you want yes/no 
 * Example of usage: pass = False, fail = True

---

## Python Datatypes cont...

* **list:** ordered collection with square brackets, i.e. []
 * You can have lists of pretty much any datatype, e.g. a list of int values, or bool values, or string values. You can even have a list of lists!
 * Useful for representing collections of data
 * Example of usage: weekdays = [“Monday”, “Tuesday”, “Wednesday”, “Thursday”, “Friday”]

---

## Python Datatypes cont...
* **dict:** unordered collection with curly brackets {}, where each element is in the form key:value, and the separator is a comma (,)
 * You can have a dictionary of pretty much any data type/s, e.g. the key might be a string, and the value an int or the key might be a float, and the value a bool
 * Useful for mapping data to labels, .e.g student to grade
 * Example of usage: student_grades = {“Sara”: 90, “Asif”: 34, “James”: 0}

---
## Python Datatypes cont...
* **set**: unordered collection with curly brackets {}, where each element is separated by a comma. Cannot contain duplicates.
 * You can have a set of pretty much any data type/s, e.g. int values, string values, bool values, etc.
 * Useful when you don’t need your data to be ordered, and don’t want any duplicates
 * Example of usage: lotto_numbers = {3, 7, 21, 33, 12, 16, 29, 2}
---

## Operators and Operands
* Python recongises certain symbols as having specific meaning, e.g. * means to multiply.
* Here are all the operators recognized by Python. We’ll be using a few of them in our programs
.center[![Python Reserved Keywords](assets/topic_03/operators.PNG)]
---
## Operators and Operands cont...
* Arithmetic Operators
.center[![Python Reserved Keywords](assets/topic_03/arithmetic.PNG)]
---

## Operators and Operands cont...
* Comparison Operators
.center[![comparison](assets/topic_03/comparison.PNG)]

---

## Operators and Operands continued....
* Logical Operators
.center[![Python Reserved Keywords](assets/topic_03/logical.PNG)]
---

## Let's Play!

* Here is the mybinder link to the Jupyter Notebook you've watched me use in this video
  *  [Visit the Jupyter Notebook](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_03%2FTopic%203%20Starting%20Out%20With%20Python%201.ipynb)
* Remember programming is all about practical experience so feel free to experiment and test things out in the notebook

* Happy coding!
.center[[![Binder Python Lesson 1](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_03%2FTopic%203%20Starting%20Out%20With%20Python%201.ipynb)]


---
## Next Topic...
* In the next video, we'll be covering control structures including:
  * if statements
  * for loops and while loops
  * continue and break

* Thanks for watching! If you have any questions, ask away on SLACK.

