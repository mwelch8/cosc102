class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 11 - Advanced Plots </h1>

<h3 class="title_headings_sml"> Dr. Mitchell Welch </h3>

---

## Reading
* [Open the notebook for this topic]()
* Take a look at the [documentation for the kernel density estimation function in Scikit-learn](https://scikit-learn.org/stable/modules/density.html#kernel-density)
* Review the [3D Plotting Tutorial](https://matplotlib.org/3.1.1/tutorials/toolkits/mplot3d.html#sphx-glr-tutorials-toolkits-mplot3d-py)


---

## Summary

* Kernel Density Estimators
* 3D Plotting

---

## Kernel Density Estimators

* Need to be careful when working with histograms.
* The size of bins and placement of the bin *edge* can dramatically impact the visualization generated.
  * This might lead to incorrect conclusions from the data.
* **Kernel Density Estimation (KDE)** can help us understand the *shape* of our data.  

---

## Kernel Density Estimators

* A formal definition of KDE is beyond the scope of this unit.
* Put simply, KDE estimates a distribution by placing a *kernel* function over each data point and summing them all together. 

.center[![Matplotlib](../assets/topic_11/kde_hist.png)]

---

## Kernel Density Estimators

* KDE isn't a silver bullet - there are still parameters to tune:
  * Kernel type/shape (this is usually a normal distribution)
  * There is a *smoothing* parameter called the *bandwidth*
* By manipulating these, you can reveal trends in the distribution of the underlying data.
* KDE is used to create impressive visualizations of distributions.
* Lets play with an [example.](plot_kde_1d.py)

---

## 3D Plotting

* So far we have only looked a 2D plots.
* Matplotlib has the capability to produce 3D plots using the `mpl_toolkits.mplot3d` module.
* You then need to create a 3D-enabled axes:

```python
fig = plt.figure(figsize=[15,10])
ax = fig.add_subplot(111, projection='3d')
```
* We will explore some 3D scatter plots: 

---

## Summary

* Kernel Density Estimators
* 3D Plotting

---
