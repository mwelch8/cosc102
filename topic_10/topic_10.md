
class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 10 - The Lifecycle of a Plot </h1>

<h3 class="title_headings_sml"> Dr. Mitchell Welch </h3>

---

## Reading
* [Open the notebook for this topic]()
* Review the [Matplotlib Lifcycle Tutorial](https://matplotlib.org/tutorials/introductory/lifecycle.html#sphx-glr-tutorials-introductory-lifecycle-py)
* Review the [Effectively Using Matplotlib Blog](https://pbpython.com/effective-matplotlib.html)

---

## Summary

* Plot Basics
* Applying Style to a Plot
* Customizing a Plot
* Combining Multiple Elements

---
## Plot Basics

.center[![Matplotlib](../assets/topic_08/anatomy.png "Plot Anatomy")]

---

## Plot Basics

* Recall: We create our figure and (1 or more) axes:

```python
# One Figure, one axes
fig, ax = plt.subplots()

# One Figure, 2 axes
fig, axs = plt.subplots(1,2)
```
* [Open the notebook for this topic and create the figure]()
---

## Applying Style to a Plot

* You can customize almost everything:

.center[![Matplotlib](../assets/topic_10/matplotlib_example.png "Plot Elements")]

.center[(Source: https://pbpython.com/effective-matplotlib.html)]

---

## Applying Style to a Plot

* Matplotlib has builtin-styles that control colour and appearance of the different plot-types.
* The `style` package adds support for easy-to-switch plotting "styles"
* [Review the style reference.](https://matplotlib.org/3.1.0/gallery/style_sheets/style_sheets_reference.html)
* [You can even define your own styles!](https://matplotlib.org/3.2.2/tutorials/introductory/customizing.html) - I'll leave this one to you!
* [Open the notebook for this topic and apply some styles]()
---

## Customizing a Plot

* Things we cna change:
  * Add labels and apply rotation.
  * Add titles.
  * 'Ticks' on the axis.
  * Adjust the limits on each axis within the plot

---

## Combining Multiple Elements

* Adding multiple plots to a single axes. (E.g. denoting a mean/standard deviation).
* Adding text and additional markers.
* Formatting labels using formatting functions.

---

## Summary

* Plot Basics
* Applying Style to a Plot
* Customizing a Plot
* Combining Multiple Elements

---
## Reading

* Review the [Matplotlib Sample Plots Tutorial](https://matplotlib.org/tutorials/introductory/sample_plots.html)
* Review the [Matplotlib Gallery (hundreds of examples!)](https://matplotlib.org/gallery/index.html)

---
