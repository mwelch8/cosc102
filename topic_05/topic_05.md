
class: bottom, left
background-image: url(assets/g.png)

<h2 class="title_headings_sml">COSC102 - Data Science Studio 1</h2>

<h1 class="title_headings_sml"> Topic 5 - Starting Out With Python #3
</h1>

<h3 class="title_headings_sml"> Andreas Shepley, PhD Candidate </h3>

---

## Slicing

* Slicing allows you to manipulate sequences such as lists and strings easily using index values
* Slicing allows you to manipulate individual items in the sequence, chunks or even the entire list!
.center[![slicing](assets/topic_05/slicing.PNG "slicing")]
---

## Index positions: what are they?
* index positions indicate the rank of a particular in a sequence
* index positions start at 0
* index positions can be negative when read from right to left
.center[![indices](assets/topic_05/indices.PNG "indices")]

---

## Accessing items by index value
* to access the value of an item at a given index, use the following syntax
.center[![indices_2](assets/topic_05/indices.PNG "indices")]
* you can change the value of an item at a given index, delete it, or perform operations with it
---

## Back to slicing!
* Here are some useful list manipulation techniques using slicing. The same techniques will work for strings.
.center[![more_slicing](assets/topic_05/more_slicing.PNG "more slicing")]
---

## zip
* The zip function allows you to iterate through several collections simultaneously 
* You can use the zip function with datatypes such as list, set and dictionaries
.center[![zip](assets/topic_05/zip.PNG "zip")]	
---

## enumerate
* The enumerate function allows you to get the index value of an item in a loop as well as the value at that index
* It can be used to add a counter to the iteration process too
.center[![enumerate](assets/topic_05/enumerate.PNG "enumerate")]
---

## A for loop in one line!
* Python allows you to write a for loop in one line. It makes it easy to perform operations on each value in a collection. For example you might want to double all the numbers in a list, like this:
.center[![one_line_loop](assets/topic_05/one_line_loop.PNG "one line loop")]
* You can even add conditions in the one line for loop, like this:
.center[![one_line_loop](assets/topic_05/one_line_loop2.PNG "one_line_loop")]	
---

## Let's Play!

* Here is the mybinder link to the Jupyter Notebook you've watched me use in this video
  *  [Visit the Jupyter Notebook](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_04%2FTopic%204%20Starting%20Out%20with%20Python%202.ipynb)
* Remember programming is all about practical experience so feel free to experiment and test things out in the notebook

* Happy coding!
.center[[![Binder Python Lesson 1](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.une.edu.au%2Fmwelch8%2Fcosc102.git/master?filepath=topic_04%2FTopic%204%20Starting%20Out%20with%20Python%202.ipynb)]


---
## Next Topic...
* In the next video, we'll be covering:
  * User-defined functions
  * Creating a program to solve a problem!
  * Documentation

* Thanks for watching! If you have any questions, ask away on SLACK.

